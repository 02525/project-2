function similarity = pearsonVectorSimilarity(u, v)
% PEARSONVECTORSIMILARITY Calculates similarity between rating vectors of
% similar based on linear correlation.
%
% Parameters:
% u: rating vector
% v: rating vector
% 
% Returns:
% similarity: scalar value of score

similarity = mean(hat(u).*hat(v));
end

function v = hat(u)
% Calculates the 'standardized' rating vector
% Uses std(u,1) to use biased standard deviation
if std(u) == 0
    msgID = 'PEARSONVECTORSIMILARITY:ZeroDiv';
    msg = 'Zero division error - standard deviation is zero.';
    Exception = MException(msgID,msg);
    throw(Exception);
end
v = (u - mean(u))/std(u,1);
end
