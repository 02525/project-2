%% Given data is defined
filmScores = [5,4,1,4;5,3,4,2;2,4,1,5;3,5,3,5;4,3,5,1;5,2,3,3;2,1,1,2;3,3,1,5];
disp("Film bed?mmelsesdata fra slide 5: ");
disp(filmScores);

%% Calculation of similarity matrix
% Similarity based on euclidean distance
euclSimMatrix = euclSimilarity(filmScores);
disp("Matrix med euclidiske similariteter: ");
disp(euclSimMatrix);

% Similarity based on pearson correlation
pearsSimMatrix = pearsonSimilarity(filmScores);
disp("Matrix med pearson similariteter: ");
disp(pearsSimMatrix);

%% Calculation of prediction
% User 4, "Andrius", is chosen
user = 4;

% Vector index of 3 nearest neighbours is calculated for user 4,
% for similiarty based on euclidean distance
% and pearson correlation respectivly

euclnn = nearestNeighbours(3, user, euclSimMatrix);
disp("Bruger 6, Sofies, 3 n?rmeste naboer i forhold til euklidisk similaritet");
disp(euclnn);

pearsnn = nearestNeighbours(3, user, pearsSimMatrix);
disp("Bruger 6, Sofies, 3 n?rmeste naboer i forhold til pearson similaritet");
disp(pearsnn);

%% Calculation of prediction
% District 9 is defined ( missing score of Andrius is set zero )
district9 = [3,4,5,0,4,4,2,2];

% Prediction based on average is calcuated for similarity based on
% euclidean distance and pearson correlation respectivly

euclidAverage = predictAverage(district9(euclnn));
disp("Forudsagt vurdering af 'District 9' ved brug af euklidisk similaritet:");
disp(euclidAverage);

pearsonAverage = predictAverage(district9(pearsnn));
disp("Forudsagt vurdering af 'District 9' ved brug af pearson similaritet:");
disp(pearsonAverage);