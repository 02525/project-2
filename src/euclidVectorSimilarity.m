function similarity = euclidVectorSimilarity(ra, rb)
% euclidVectorSimilarity  Find the euclidian distance between two vectors.
similarity = 1/(1+norm(ra-rb));