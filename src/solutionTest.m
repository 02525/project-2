% test data from the slides
michael = [5,4,1,4];
christian = [5,3,4,2];
gitte = [2,4,1,5];
andrius = [3,5,3,5];
emilie = [4,3,5,1];
sofie = [5,2,3,3];
isabel = [2,1,1,2];
wivi = [3,3,1,5];

allThePeople = [
    michael;
    christian;
    gitte; 
    andrius; 
    emilie;
    sofie;
    isabel;
    wivi
];

euclidSolutionMatrix = [
    [1,0.21,0.24,0.24,0.16,0.25,0.18,0.29];
    [0.21,1,0.16,0.19,0.37,0.37,0.18,0.18];
    [0.24,0.16,1,0.29,0.14,0.18,0.19,0.41];
    [0.24,0.19,0.29,1,0.17,0.2,0.15,0.26];
    [0.16,0.37,0.14,0.17,1,0.24,0.17,0.15];
    [0.25,0.37,0.18,0.2,0.24,1,0.21,0.22];
    [0.18,0.18,0.19,0.15,0.17,0.21,1,0.21];
    [0.29,0.18,0.41,0.26,0.15,0.22,0.21,1]
];


pearsonSolutionMatrix = [
    [1.0,0,0.53,0.33,-0.51,0.38,0.67,0.71];
    [0,1,-0.85,-0.89,0.83,0.72,0,-0.63];
    [0.53,-0.85,1,0.95,-0.96,-0.44,0.32,0.89];
    [0.33,-0.89,0.95,1,-0.85,-0.69,0,0.71];
    [-0.51,0.83,-0.96,-0.85,1,0.27,-0.51,-0.96];
    [0.38,0.72,-0.44,-0.69,0.27,1,0.69,0];
    [0.67,0,0.32,0,-0.51,0.69,1,0.71];
    [0.71,-0.63,0.89,0.71,-0.96,0,0.71,1]
];

% our code
euclid = @(v1, v2) round(euclidVectorSimilarity(v1, v2),2);
pearson = @(v1, v2) round(pearsonVectorSimilarity(v1,v2),2);
    
%% Test 0: Math works
assert(3+5 == 5+3, "RUN RUN RUN, addition doesn't commute")


%% Test 1: Testing if similarity function commute and return 1 on two equal inputs
for person = allThePeople
	assert(euclid(person,person) == 1, "Euclid similarity does not equal 1 with two equal inputs")
    assert(pearson(person,person) == 1, "Pearson similarity does not equal 1 with two equal inputs")
    for person2 = allThePeople
        assert(euclid(person,person2) == euclid(person2, person), "Euclid similarity does not commute")
        assert(pearson(person,person2) == pearson(person2, person), "Pearson simmilarity does not commute")
    end
end

%% Test 2: Euclid similarity -  Testing known values
similarityExpectation(euclid, "Gitte", gitte, "Andrius", andrius, 0.29);
similarityExpectation(euclid, "Emilie", emilie, "Andrius", andrius, 0.17);
similarityExpectation(euclid, "Emilie", emilie, "Gitte", gitte, 0.14);


%% Test 3: Pearson similarity -  Testing known values
similarityExpectation(pearson, "Gitte", gitte, "Andrius", andrius, 0.95);
similarityExpectation(pearson, "Emilie", emilie, "Andrius", andrius, -0.85);
similarityExpectation(pearson, "Emilie", emilie, "Gitte", gitte, -0.96);


%% Test 4: Euclid solution matrix

calculatedEuclidean = round(euclSimilarity(allThePeople), 2);
assert(any(calculatedEuclidean == euclidSolutionMatrix, [1,2]), "The euclid solution does not match")

%% Test 5: Pearson solution matrix
calculatedPearson = round(pearsonSimilarity(allThePeople), 2);
assert(any(calculatedPearson == pearsonSolutionMatrix, [1,2]), "The pearson solution does not match")


%% Test 6: Nearest neighbours
calculatedNearestNeighboursAndrius = nearestNeighbours(3, 4, euclidSolutionMatrix);
assert(any(calculatedNearestNeighboursAndrius == [1,0,1,0,0,0,0,1]), "Nearest neighbours does not match")

%% Utility functions 
% Tests if two persons have an expected similarity. Gives a nice error
% message if they don't.
function testResult = similarityExpectation(similarityFunction, name1, person1, name2, person2, expectation)
similarity = round(similarityFunction(person1, person2),2);
assert(similarity == expectation, "Similarity between " + name1 + " and " + name2 + " returns " + similarity + " but "+ expectation + " was expected")
end