function similarity = pearsonSimilarity(filmScores)
% pearsonSimilarity  Create a matrix of pearson similarities between users.
% 
% Parameters:
% filmScores: A matrix of film ratings with each row holding a users
% ratings.
%
% Returns:
% similarity: A symmetric matrix of pearson similarities between users.
similarity = matrixSimilarity(@(v1, v2) pearsonVectorSimilarity(v1, v2), filmScores);