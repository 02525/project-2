function nearest = nearestNeighbours (n, user, similarityMatrix)
% nearestNeighbours  Finds the given users n nearest neighbours.
% 
% Parameters:
% n: Number of nearest neighbours.
% user: The index of the user whose neighbours to find.
% similarityMatrix: A matrix of similarity values like the one given by
% matrixSimilariy. Must be square.
%
% Returns:
% nearest: An array with true in all indices associated with a user that is
% a nearest neighbour.

if diff(size(similarityMatrix)) ~= 0
    msgID = 'NEARESTNEIGHBOURS:SimilarityMatrixNotSquare';
    msg = 'similarityMatrix must be square';
    Exception = MException(msgID,msg);
    throw(Exception);
end

% Get similarities for user
sim = similarityMatrix(user, : );

% Sort similarities
sortedSim = sort(sim, 'descend');

% Get n nearest neighbours
nearest = sortedSim(2:n+1);

% Get max distance <=> minimum similarity
minSim = min(nearest);

% Return vector of whether user is in neighbourhood
nearest = sim >= minSim;
nearest(user) = 0;