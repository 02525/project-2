function similarity = euclSimilarity(filmScores)
% pearsonSimilarity  Create a matrix of euclidean similarities between users.
% 
% Parameters:
% filmScores: A matrix of film ratings with each row holding a users
% ratings.
%
% Returns:
% similarity: A symmetric matrix of pearson similarities between users.
similarity = matrixSimilarity(@(v1, v2) euclidVectorSimilarity(v1, v2), filmScores);