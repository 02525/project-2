% Calculates the similarity matrix given the a function that caculates
% the similarity between two vectors and a matrix containing
% Parameters:
%   simFunc: Lambda from a pair of (1 x N) vectors to a float
%   dataMatrix: An (M x N) matrix 
%
% Returns:
%   similarityMatrix: An (M x M) matrix

function similarityMatrix = matrixSimilarity (simFunc, dataMatrix)
% Get size of matrix
[M, N] = size(dataMatrix);

% Define matrix size M x M
similarityMatrix = ones(M,M);

% Get all pairs of users
combination = nchoosek(1:M, 2);

% Calculate elments of similarity matrix
for indices = combination.'
    % Get index of user i and j
    i = indices(1);
    j = indices(2);
    
    % Get rating vectors of users
    u = dataMatrix(i, : );
    v = dataMatrix(j, : );
    
    % Set elment (i,j) and (j,i) of similarity
    %score to similarity of user i and user j
    similarityMatrix(i, j) = simFunc(u, v);
    similarityMatrix(j, i) = simFunc(u, v);
end
